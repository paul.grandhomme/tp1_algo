#include <stdio.h>
#include <stdlib.h>

#include "abrAVL.h"

void afficher_arbre (Arbre_AVL_t a, int niveau)
{
  int i ;
  
  if (a != NULL){
	afficher_arbre (a->fdroite,niveau+1) ;
	
	for (i = 0; i < niveau; i++)
	  printf ("\t") ;
	printf (" %d[%d] (%d)\n\n", a->cle, a->balance, niveau) ;

	afficher_arbre (a->fgauche, niveau+1) ;
      }
  return ;
}


int hauteur(Arbre_AVL_t a){
	if(a==NULL)return -1;
	int hauteur_gauche=0;
	if(a->fgauche!=NULL) hauteur_gauche=hauteur(a->fgauche);
	int hauteur_droite=0;
	if(a->fdroite!=NULL) hauteur_droite=hauteur(a->fdroite);

	if(a->fgauche==NULL && a->fdroite==NULL)return 0;
	if(a->fgauche==NULL) return hauteur_droite+1;
	if(a->fdroite==NULL) return hauteur_gauche+1;
	if(hauteur_droite>hauteur_gauche)return hauteur_droite+1;
	return hauteur_gauche+1;
}

Arbre_AVL_t nouveau_noeud(Arbre_AVL_t a, int cle, Arbre_AVL_t b){
	Arbre_AVL_t new=malloc(sizeof(Arbre_AVL_t));
 	new->cle=cle;
 	new->fgauche=a;
 	new->fdroite=b;
 	new->balance=0;
 	int hg=hauteur(a), hd=hauteur(b);
 	if(hg!=-1 || hd!=-1){
 		if(hg!=-1 && hd==-1)new->balance=hg+1;
 		else if(hg==-1 && hd!=-1)new->balance=hd+1;
 		else {
 			new->balance=hg-hd;
 		}
 	}
 	if(new->balance>2)new->balance=2;
 	if(new->balance<-2)new->balance=-2;
 	return new;
}

Arbre_AVL_t rotation_gauche(Arbre_AVL_t a){
	if(a==NULL || a->fdroite==NULL){
		printf("rotation_gauche impossible\n");
		return a;
	}
	Arbre_AVL_t b=nouveau_noeud(nouveau_noeud(a->fgauche, a->cle, a->fdroite->fgauche), a->fdroite->cle, a->fdroite->fdroite);
	return b;
}

Arbre_AVL_t rotation_droite(Arbre_AVL_t a){
	if(a==NULL || a->fgauche==NULL){
		printf("rotation_droite impossible\n");
		return a;
	}
	Arbre_AVL_t b=nouveau_noeud(a->fgauche->fgauche, a->fgauche->cle, nouveau_noeud(a->fgauche->fdroite,a->cle,a->fdroite));
	return b;
}

Arbre_AVL_t double_rotation_gauche(Arbre_AVL_t a){
	if(a==NULL || a->fdroite==NULL || a->fdroite->fgauche==NULL){
		printf("double rotation gauche impossible\n");
		return a;
	}
	return nouveau_noeud(nouveau_noeud(a->fgauche,a->cle,a->fdroite->fgauche->fgauche),
											a->fdroite->fgauche->cle,
						nouveau_noeud(a->fdroite->fgauche->fdroite,a->fdroite->cle,a->fdroite->fdroite));
}

Arbre_AVL_t double_rotation_droite(Arbre_AVL_t a){
	if(a==NULL || a->fgauche==NULL || a->fgauche->fdroite==NULL){
		printf("double rotation droite impossible\n");
		return a;
	}
	return nouveau_noeud(nouveau_noeud(a->fgauche->fgauche,a->fgauche->cle,a->fgauche->fdroite->fgauche),
											a->fgauche->fdroite->cle,
						nouveau_noeud(a->fgauche->fdroite->fdroite,a->cle,a->fdroite));
}

Arbre_AVL_t reequilibrage(Arbre_AVL_t a){
	if(a->balance==-2){
		if(a->fdroite->balance <=0) {
			return rotation_gauche(a);
		}else{
			a->fdroite=rotation_droite(a->fdroite);
			return rotation_gauche(a);
		}
	}else if(a->balance==2){
		if(a->fgauche->balance >=0) return rotation_droite(a);
		else{
			a->fgauche=rotation_gauche(a->fgauche);
			return rotation_droite(a);
		}
	}else return a;
}

int inserer_cle_arbre(Arbre_AVL_t a, int c){
  if(a==NULL){
    a = nouveau_noeud(NULL, c, NULL);
    return 0;
  } 
  if(a->cle==c) return 1;
  if(a->cle>c){
    if(a->fgauche==NULL){
      a->fgauche=nouveau_noeud(NULL, c, NULL);
      return 1;
    }else{
      return inserer_cle_arbre(a->fgauche, c);
    }
  }
  if(a->fdroite==NULL){
    a->fdroite=nouveau_noeud(NULL, c, NULL);
    return 1;
  }else{
    return inserer_cle_arbre(a->fdroite, c);
  }
  return 0;
}

void ajout_cle(Arbre_AVL_t a, int cle){
	if(a==NULL) {
    	a = nouveau_noeud(NULL, cle, NULL);
	}
	if(a->cle>cle) {
		if(a->fgauche==NULL) {
			a->fgauche=nouveau_noeud(NULL, cle, NULL);
		} else {
			inserer_cle_arbre(a->fgauche, cle);
		}
	}
	if(a->fdroite==NULL) {
		a->fdroite=nouveau_noeud(NULL, cle, NULL);
	}else{
		inserer_cle_arbre(a->fdroite, cle);
	}
	reequilibrage(a);
}

int arbre_contient_cle(Arbre_AVL_t a, int c){
  if(a==NULL) return 0;
  if(a->cle==c) return 1;
  return (arbre_contient_cle(a->fgauche, c)||arbre_contient_cle(a->fdroite, c));
}

void destruction_cle(Arbre_AVL_t a, int cle){
	if (arbre_contient_cle(a, cle)==1) { //Si la cle est dans l'arbre
		Arbre_AVL_t abr = a;
		if(abr->cle == cle){
		a=abr->fdroite;
		a->fgauche=abr->fgauche;
		}else{
		//tant que les fils ne contiennent pas la cle
		while((abr->fgauche==NULL || (abr->fgauche!=NULL && abr->fgauche->cle!=cle)) && (abr->fdroite==NULL || (abr->fdroite!=NULL && abr->fdroite->cle!=cle))){
			if(abr->cle<cle)abr=abr->fdroite;
			else abr=abr->fgauche;
		}
		int gauche = (abr->fgauche!=NULL && abr->fgauche->cle==cle);
		Arbre_AVL_t g=NULL, d=NULL;
		if(gauche){
			if ((abr->fgauche!=NULL) && (abr->fgauche->fgauche!=NULL)) {
			g=abr->fgauche->fgauche;
			}
			if ((abr->fgauche!=NULL) && (abr->fgauche->fdroite!=NULL)) {
			d=abr->fgauche->fdroite;
			}
			if (g!=NULL) {
			abr->fgauche=g;
			}
			if (abr->fdroite!=NULL) {
			abr->fdroite=abr->fdroite;
			}
		}else{
			if ((abr->fdroite!=NULL) && (abr->fdroite->fgauche!=NULL)) {
			g=abr->fdroite->fgauche;
			}
			if ((abr->fdroite!=NULL) && (abr->fdroite->fdroite!=NULL)) {
			d=abr->fdroite->fdroite;
			}
			if (d!=NULL) {
			abr->fdroite=d;
			}
			if (abr->fgauche!=NULL) {
			abr->fgauche=abr->fgauche;
			}
		}
		if(gauche){
			if ((g!=NULL)&&(d!=NULL)) {
				while (g->cle > abr->fgauche->cle) {
					abr=abr->fgauche;
				}
				abr->fgauche->fdroite=d;
			} else {
				abr->fgauche=NULL;
			}
		}else{
			if (g!=NULL) {
				while (g->cle > abr->fdroite->cle) {
					abr=abr->fdroite;
				}
				abr->fdroite->fgauche=g;
			} else {
				abr->fdroite=NULL;
			}
		}
		}
		a = abr;
	}
}