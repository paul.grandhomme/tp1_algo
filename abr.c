#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "abr.h"
#include "pile.h"
#include "file.h"



#define max(a,b) ((a)>(b)?(a):(b))


int feuille (Arbre_t a)
{
  if (a == NULL)
    return 0 ;
  else
    {
      if ((a->fgauche == NULL) && (a->fdroite == NULL))
	return 1 ;
      else
	return 0 ;
    }
}

Arbre_t ajouter_noeud (Arbre_t a, Arbre_t n)
{
  /* ajouter le noeud n dans l'arbre a */
  
  if (a == NULL)
    return n ;
  else if (n->cle < a->cle)
	a->fgauche = ajouter_noeud (a->fgauche, n) ;
  else
	a->fdroite = ajouter_noeud (a->fdroite, n) ;
  return a ;
  
}  

Arbre_t rechercher_cle_arbre (Arbre_t a, int valeur)
{
  if (a == NULL)
    return NULL ;
  else
    {
      if (a->cle == valeur)
	return a ;
      else
	{
	  if (a->cle < valeur)
	    return rechercher_cle_arbre (a->fdroite, valeur) ;
	  else
	    return rechercher_cle_arbre (a->fgauche, valeur) ;
	}
    }
}

Arbre_t ajouter_cle (Arbre_t a, int cle)
{
  Arbre_t n ;
  Arbre_t ptrouve ;
  
  /* 
     ajout de la clé. Creation du noeud n qu'on insere 
    dans l'arbre a
  */

  ptrouve = rechercher_cle_arbre (a, cle) ;

  if (ptrouve == NULL)
    {
      n = (Arbre_t) malloc (sizeof(noeud_t)) ;
      n->cle = cle;
      n->fgauche = NULL ;
      n->fdroite = NULL ;

      a = ajouter_noeud (a, n) ;
      return a ;
    }
  else
    return a ;
}


Arbre_t lire_arbre (char *nom_fichier)
{
  FILE *f ;
  int cle;
  Arbre_t a = NULL;
  
  f = fopen (nom_fichier, "r") ;

  while (fscanf (f, "%d", &cle) != EOF)
    {
      a = ajouter_cle (a, cle) ;
    }
    
  fclose (f) ;

  return a ;
}

void afficher_arbre (Arbre_t a, int niveau)
{
  /*
    affichage de l'arbre a
    on l'affiche en le penchant sur sa gauche
    la partie droite (haute) se retrouve en l'air
  */
  
  int i ;
  
  if (a != NULL)
      {
	afficher_arbre (a->fdroite,niveau+1) ;
	
	for (i = 0; i < niveau; i++)
	  printf ("\t") ;
	printf (" %d (%d)\n\n", a->cle, niveau) ;

	afficher_arbre (a->fgauche, niveau+1) ;
      }
  return ;
}

//Renvoie la hauteur de l'arbre a de maniere recursive
int hauteur_arbre_r (Arbre_t a)
{
  if (a==NULL) {
    return 0;
  } else if ((a->fgauche!=NULL) || (a->fdroite!=NULL)) {
    return 1 + max(hauteur_arbre_r(a->fgauche), hauteur_arbre_r(a->fdroite));
  } else {
    return max(hauteur_arbre_r(a->fgauche), hauteur_arbre_r(a->fdroite));
  }
}

//Renvoie la hauteur de l'arbre a de maniere recursive
int hauteur_arbre_nr (Arbre_t a)
{
  if(a==NULL)return 0;
  int h=0, taille=1, proTaille=1;
  pfile_t file = creer_file();
  enfiler(file, a);
  while(!file_vide(file)){
    h++;
    taille = proTaille;
    proTaille=0;
    for(int i=0; i<taille; i++){
      Arbre_t n = defiler(file);
      if(n->fgauche!=NULL){
        enfiler(file, n->fgauche);
        proTaille++;
      }
      if(n->fdroite!=NULL){
        enfiler(file, n->fdroite);
        proTaille++;
      }
    }
  }
  return h-1; //car on le premier niveau de l'arbre vaut 0, sinon on peut mettre h=-1 au moment de l'initialisation
}

//ecrit les cle de chaque noeud niveau par niveau
void parcourir_arbre_largeur (Arbre_t a)
{
  if(a==NULL)return ;
  int taille=1, proTaille=1;
  pfile_t file = creer_file();
  enfiler(file, a);
  while(!file_vide(file)){
    taille = proTaille;
    proTaille=0;
    for(int i=0; i<taille; i++){
      Arbre_t n = defiler(file);
      printf("%d ", n->cle);
      if(n->fgauche!=NULL){
        enfiler(file, n->fgauche);
        proTaille++;
      }
      if(n->fdroite!=NULL){
        enfiler(file, n->fdroite);
        proTaille++;
      }
    }
  }
}

//affiche combien il y a de noeud dans chaque niveau
void afficher_nombre_noeuds_par_niveau (Arbre_t a)
{
  if(a==NULL)return ;
  int taille=1, proTaille=1;
  pfile_t file = creer_file();
  enfiler(file, a);
  while(!file_vide(file)){
    taille = proTaille;
    printf("%d ", taille);
    proTaille=0;
    for(int i=0; i<taille; i++){
      Arbre_t n = defiler(file);
      if(n->fgauche!=NULL){
        enfiler(file, n->fgauche);
        proTaille++;
      }
      if(n->fdroite!=NULL){
        enfiler(file, n->fdroite);
        proTaille++;
      }
    }
  }
}


//renvoie le nombre de cle dans cet arbre de maniere recursive
int nombre_cles_arbre_r (Arbre_t a)
{
  if(a==NULL)return 0;
  return 1+nombre_cles_arbre_r(a->fgauche)+nombre_cles_arbre_r(a->fdroite);
}

//renvoie le nombre de cle dans cet arbre de maniere non recursive
int nombre_cles_arbre_nr (Arbre_t a)
{
  if(a==NULL)return 0;
  int taille=1, proTaille=1, nbr=1;
  pfile_t file = creer_file();
  enfiler(file, a);
  while(!file_vide(file)){
    nbr+=taille;
    taille = proTaille;
    proTaille=0;
    for(int i=0; i<taille; i++){
      Arbre_t n = defiler(file);
      if(n->fgauche!=NULL){
        enfiler(file, n->fgauche);
        proTaille++;
      }
      if(n->fdroite!=NULL){
        enfiler(file, n->fdroite);
        proTaille++;
      }
    }
  }
  return nbr;
}

//renvoie la cle avec la valeur minimum
int trouver_cle_min (Arbre_t a)
{
  if(a==NULL)return -1;
  int g=trouver_cle_min(a->fgauche), d=trouver_cle_min(a->fdroite);
  int min = a->cle;
  if(g!=-1 && g<min) min=g;
  if(d!=-1 && d<min) min=d;
  return min; 
}

//fonction annexe
//remplie le tableau avec toutes les cle dans l'ordre de maniere recursive
int liste_cle_r(Arbre_t a, int index, int *tab){
  if(a==NULL) return index;
  if(a->fgauche!=NULL){
    index=liste_cle_r(a->fgauche, index, tab);
  }
  tab[index]=a->cle;
  if(a->fdroite!=NULL) {
    return index=liste_cle_r(a->fdroite, index+1, tab);
  } else {
    return index+1;
  }
} 

//ecrit les cles de l'arbre dans l'ordre croissant de maniere recursive
void imprimer_liste_cle_triee_r (Arbre_t a)
{
  int taille = nombre_cles_arbre_r(a);
  int tab[taille];
  liste_cle_r(a, 0, tab);

  for(int i=0; i<taille; i++) printf("%d ", tab[i]);
}

//fonction annexe
//remplie le tableau avec toutes les cle non ordonne de maniere non recursive
void liste_cle_nr(Arbre_t a, int *tab){
  int index=0;
  if(a==NULL)return;
  int taille=1, proTaille=1;
  pfile_t file = creer_file();
  enfiler(file, a);
  while(!file_vide(file)){
    taille = proTaille;
    proTaille=0;
    for(int i=0; i<taille; i++){
      Arbre_t n = defiler(file);
      tab[index]=n->cle;
      index++;
      if(n->fgauche!=NULL){
        enfiler(file, n->fgauche);
        proTaille++;
      }
      if(n->fdroite!=NULL){
        enfiler(file, n->fdroite);
        proTaille++;
      }
    }
  }
}

//ecrit les cles de l'arbre dans l'ordre croissant de maniere non recursive
void imprimer_liste_cle_triee_nr (Arbre_t a)
{
  int taille = nombre_cles_arbre_nr(a);
  int tab[taille];
  liste_cle_nr(a, tab);

  //tri tableau
  for(int i=0; i<taille; i++){
    for(int y=i; y<taille; y++){
      if(tab[i]>tab[y]){
        int tmp = tab[i];
        tab[i]=tab[y];
        tab[y]=tmp;
      }
    }
  }

  for(int i=0; i<taille; i++) printf("%d ", tab[i]);
}

//renvoie si cle dernier niveau est rempli
int arbre_plein (Arbre_t a)
{
  int hauteur = hauteur_arbre_r(a);
  int nbr_noeud = nombre_cles_arbre_r(a);
  int res = 0;
  for (int i=1; i<=hauteur; i++) {
    res = res + pow(2, i);
  }
  return (nbr_noeud==res+1);
}

//fonction: arbre_parfait
//données: on lui donne un arbre
//resultat: renvoie 1 si l'arbre est parfait, 0 sinon 
int arbre_parfait(Arbre_t a)
{
  if (a->fdroite == NULL && a->fgauche == NULL)
  {
    return 1;
  }
  else if (a->fdroite != NULL && a->fgauche == NULL)
  {
    return 0;
  }
  else if (a->fdroite == NULL && a->fgauche != NULL)
  {
    if (((a->fgauche)->fgauche == NULL) && ((a->fgauche)->fdroite == NULL))
      return 1;
    else
      return 0;
  }
  else
  {
    return arbre_parfait(a->fgauche) && arbre_parfait(a->fdroite);
  }
}



//fonction: nouveau_noeud
//données: on lui donne deux arbre et une etiquette
//resultat: un arbre formé à partir des données
Arbre_t nouveau_noeud(Arbre_t a, int valeur,Arbre_t b){
  Arbre_t new=malloc(sizeof(Arbre_t));
  new->cle=valeur;
  new->fgauche=a;
  new->fdroite=b;
  return new;
}

//fonction: rechercher_cle_sup_arbre
//données: on lui donne un arbre et une valeur v
//resultat: un arbre formé à partir des valeurs strictements supérieures à v
Arbre_t rechercher_cle_sup_arbre(Arbre_t a, int valeur)
{
  if (a == NULL)
    return a;
  else if (a->cle > valeur)
  {
    return nouveau_noeud(rechercher_cle_sup_arbre(a->fgauche, valeur), a->cle, a->fdroite);
  }
  else if (a->cle < valeur)
  {
    return rechercher_cle_sup_arbre(a->fdroite, valeur);
  }
  else
  {
    return a->fdroite;
  }
}

//fonction: rechercher_cle_inf_arbre
//données: on lui donne un arbre et une valeur v
//resultat: un arbre formé à partir des valeurs strictements inferieures à v
Arbre_t rechercher_cle_inf_arbre(Arbre_t a, int valeur)
{
  if (a == NULL)
    return a;
  else if (a->cle < valeur)
  {
    return nouveau_noeud(a->fgauche, a->cle, rechercher_cle_inf_arbre(a->fdroite, valeur));
  }
  else if (a->cle > valeur)
  {
    return rechercher_cle_inf_arbre(a->fgauche, valeur);
  }
  else
  {
    return a->fgauche;
  }
}



//enleve le noeud qui contient cette cle en recolle ses fils a l'arbre
Arbre_t detruire_cle_arbre (Arbre_t a, int cle)
{
  if (rechercher_cle_arbre(a, cle)) {
    if(a==NULL)return a;
    Arbre_t abr = a;
    if(abr->cle == cle){
      a=abr->fdroite;
      a->fgauche=abr->fgauche;
    }else{
      //tant que les fils ne contiennent pas la cle
      while((abr->fgauche==NULL || (abr->fgauche!=NULL && abr->fgauche->cle!=cle)) && (abr->fdroite==NULL || (abr->fdroite!=NULL && abr->fdroite->cle!=cle))){
        if(abr->cle<cle)abr=abr->fdroite;
        else abr=abr->fgauche;
      }
      int gauche = (abr->fgauche!=NULL && abr->fgauche->cle==cle);
      Arbre_t g=NULL, d=NULL;
      if(gauche){
        if ((abr->fgauche!=NULL) && (abr->fgauche->fgauche!=NULL)) {
          g=abr->fgauche->fgauche;
        }
        if ((abr->fgauche!=NULL) && (abr->fgauche->fdroite!=NULL)) {
          d=abr->fgauche->fdroite;
        }
        if (g!=NULL) {
          abr->fgauche=g;
        }
        if (abr->fdroite!=NULL) {
          abr->fdroite=abr->fdroite;
        }
      }else{
        if ((abr->fdroite!=NULL) && (abr->fdroite->fgauche!=NULL)) {
          g=abr->fdroite->fgauche;
        }
        if ((abr->fdroite!=NULL) && (abr->fdroite->fdroite!=NULL)) {
          d=abr->fdroite->fdroite;
        }
        if (d!=NULL) {
          abr->fdroite=d;
        }
        if (abr->fgauche!=NULL) {
          abr->fgauche=abr->fgauche;
        }
      }
      if(gauche){
        if ((g!=NULL)&&(d!=NULL)) {
          while (g->cle > abr->fgauche->cle) {
            abr=abr->fgauche;
          }
          abr->fgauche->fdroite=d;
        }
      }else{
        if (g!=NULL) {
          while (g->cle > abr->fdroite->cle) {
            abr=abr->fdroite;
          }
          abr->fdroite->fgauche=g;
        }
      }
    }
  }
  
  return a;
}

/**
 * fonction: arbre_contient_cle
 * données: un arbre a et une cle c
 * resultat: 1 si la cle est dans l'arbre, 0 sinon
 */
int arbre_contient_cle(Arbre_t a, int c){
  if(a==NULL) return 0;
  if(a->cle==c) return 1;
  return (arbre_contient_cle(a->fgauche, c)||arbre_contient_cle(a->fdroite, c));
}


/**
 * fonction: inserer_cle_arbre
 * données: un arbre a et une cle c
 * resultat: 1 si la cle a été insérée avec succès (et insere la cle c dans l'arbre a)
 */
int inserer_cle_arbre(Arbre_t a, int c){
  if(a==NULL){
    a = nouveau_noeud(NULL, c, NULL);
    return 0;
  } 
  if(a->cle==c) return 1;
  if(a->cle>c){
    if(a->fgauche==NULL){
      a->fgauche=nouveau_noeud(NULL, c, NULL);
      return 1;
    }else{
      return inserer_cle_arbre(a->fgauche, c);
    }
  }
  if(a->fdroite==NULL){
    a->fdroite=nouveau_noeud(NULL, c, NULL);
    return 1;
  }else{
    return inserer_cle_arbre(a->fdroite, c);
  }
  return 0;
}



/*
fonction: intersection_deux_arbres
données: deux arbres a1 et a2
resultat: un arbre qui contient les clés présentes dans a1 ET a2
*/
Arbre_t intersection_deux_arbres (Arbre_t a1, Arbre_t a2)
{
  if(a1==NULL || a2==NULL) return NULL;
  Arbre_t res = nouveau_noeud(NULL, -1, NULL);
  int initialise=0;

  Arbre_t a = a1;
  int taille=1, proTaille=1;
  pfile_t file = creer_file();
  if(a!=NULL) enfiler(file, a);
  while(!file_vide(file)){
    taille = proTaille;
    proTaille=0;
    for(int i=0; i<taille; i++){
      Arbre_t n = defiler(file);
      if(arbre_contient_cle(a2, n->cle)) {
        if(initialise) inserer_cle_arbre(res, n->cle);
        else {
          initialise = 1;
          res->cle = n->cle;
        }
      }
      if(n->fgauche!=NULL){
        enfiler(file, n->fgauche);
        proTaille++;
      }
      if(n->fdroite!=NULL){
        enfiler(file, n->fdroite);
        proTaille++;
      }
    }
  }
  if(!initialise) return NULL;
  return res;
}

/*
fonction: union_deux_arbres
données: deux arbres a1 et a2
resultat: un arbre qui contient les clés présentes dans a1 OU a2
*/
Arbre_t union_deux_arbres (Arbre_t a1, Arbre_t a2)
{
  if(a1==NULL && a2==NULL)return NULL;
  if(a1==NULL) return a2;
  if(a2==NULL) return a1;
  Arbre_t res = nouveau_noeud(NULL, a1->cle, NULL);

  //copie de l'arbre a1 dans le resultat
  Arbre_t a = a1;
  int taille=1, proTaille=1;
  pfile_t file = creer_file();
  if(a->fgauche!=NULL) enfiler(file, a->fgauche);
  if(a->fdroite!=NULL) enfiler(file, a->fdroite);
  while(!file_vide(file)){
    taille = proTaille;
    proTaille=0;
    for(int i=0; i<taille; i++){
      Arbre_t n = defiler(file);
      inserer_cle_arbre(res, n->cle);
      if(n->fgauche!=NULL){
        enfiler(file, n->fgauche);
        proTaille++;
      }
      if(n->fdroite!=NULL){
        enfiler(file, n->fdroite);
        proTaille++;
      }
    }
  }

  //ajout des noeuds de a2 à res
  a = a2;
  taille=1, proTaille=1;
  file = creer_file();
  enfiler(file, a);
  while(!file_vide(file)){
    taille = proTaille;
    proTaille=0;
    for(int i=0; i<taille; i++){
      Arbre_t n = defiler(file);
      if(!arbre_contient_cle(res, n->cle)) inserer_cle_arbre(res, n->cle);
      if(n->fgauche!=NULL){
        enfiler(file, n->fgauche);
        proTaille++;
      }
      if(n->fdroite!=NULL){
        enfiler(file, n->fdroite);
        proTaille++;
      }
    }
  }
  return res;

}