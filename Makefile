all: test_abr test_abr_AVL

abr.o: abr.c abr.h pile.h file.h
	gcc -Wall -c abr.c

abrAVL.o: abrAVL.c abrAVL.h
	gcc -Wall -c abrAVL.c

pile.o: pile.h pile.c
	gcc -Wall -c pile.c

file.o: file.h file.c
	gcc -Wall -c file.c

test_abr.o: test_abr.c abr.h file.h
	gcc -Wall -c test_abr.c

test_abr_AVL.o: test_abr_AVL.c abrAVL.h
	gcc -Wall -c test_abr_AVL.c

test_abr: test_abr.o pile.o file.o abr.o
	gcc -o test_abr abr.o pile.o file.o test_abr.o -lm

test_abr_AVL: test_abr_AVL.o abrAVL.o
	gcc -o test_abr_AVL abrAVL.o test_abr_AVL.o

clean:
	rm -f *.o *~ test_abr test_abr_AVL


