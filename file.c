#include <stdlib.h>
#include <stdio.h>

#include "abr.h"
#include "file.h"

pfile_t creer_file ()
{
  pfile_t file = malloc(sizeof(file_t));
  file->tete=0;
  file->queue=0;
  return file;
}

int detruire_file (pfile_t f)
{
  free(f);
  return 0;
}

void afficher_file(pfile_t f){
  if(file_vide(f))printf("La file est vide\n");
  if(file_pleine(f))printf("La file est pleine\n");
  printf("File : t=%d q=%d\n[", f->tete, f->queue);
  for(int i=0; i<f->tete-f->queue; i++){
    printf("%d ", (f->Tab[(f->queue+i)%MAX_FILE_SIZE])->cle);
  }
  printf("]\n\n");
}


int file_vide (pfile_t f)
{
  return (f->tete==f->queue);
}

int file_pleine (pfile_t f)
{
  return (f->tete!=f->queue && f->tete%MAX_FILE_SIZE==f->queue);
}

pnoeud_t defiler (pfile_t f)
{
  if(f==NULL)return NULL;
  pnoeud_t var = f->Tab[f->queue];
  f->queue = (f->queue+1)%MAX_FILE_SIZE;
  if(f->queue==0)f->tete=f->tete%MAX_FILE_SIZE;
  return var;
}

int enfiler (pfile_t f, pnoeud_t p)
{
  f->Tab[f->tete%MAX_FILE_SIZE]=p;
  f->tete++;
  return 0;
}
