typedef struct n {
  int balance;
  int cle;
  struct n *fgauche, *fdroite;
} noeud_AVL_t, *pnoeud_AVL_t ;

typedef pnoeud_AVL_t Arbre_AVL_t;

void afficher_arbre (Arbre_AVL_t a, int niveau);

int hauteur(Arbre_AVL_t a);

Arbre_AVL_t nouveau_noeud(Arbre_AVL_t a, int cle, Arbre_AVL_t b);

Arbre_AVL_t rotation_gauche(Arbre_AVL_t a);

Arbre_AVL_t rotation_droite(Arbre_AVL_t a);

Arbre_AVL_t double_rotation_gauche(Arbre_AVL_t a);

Arbre_AVL_t double_rotation_droite(Arbre_AVL_t a);

Arbre_AVL_t reequilibrage(Arbre_AVL_t a);

int inserer_cle_arbre(Arbre_AVL_t a, int c);

void ajout_cle(Arbre_AVL_t a, int cle);

int arbre_contient_cle(Arbre_AVL_t a, int c);

void destruction_cle(Arbre_AVL_t a, int cle);