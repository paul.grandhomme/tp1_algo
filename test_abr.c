#include <stdio.h>
#include <stdlib.h>

#include "abr.h"
#include "file.h"
#include "pile.h"

#define N 6
#define C 6

void testArbre(Arbre_t a)
{
  //affichage en largeur
  printf("Affichage en largeur:\n");
  parcourir_arbre_largeur(a);

  //Hauteur de l'arbre a
  if (hauteur_arbre_r(a) == hauteur_arbre_nr(a))
  {
    printf("\nLa fonction non récursive et récursive donne la même hauteur: ");
    printf("%d\n", hauteur_arbre_nr(a));
  }
  else
  {
    printf("nr : %d\n", hauteur_arbre_nr(a));
    printf("r : %d\n", hauteur_arbre_r(a));
    printf("ERREUR: La fonction non récursive et récursive ne donne pas la même hauteur...\n");
  }

  //Nombre de noeud dans l'arbre a
  printf("Nombre de noeuds par niveau: ");
  afficher_nombre_noeuds_par_niveau(a);
  printf("\n");

  //Nombre de clef dans l'arbre a
  if (nombre_cles_arbre_nr(a) == nombre_cles_arbre_r(a))
  {
    printf("La fonction non récursive et récursive donne le même nombre de clef: ");
    printf("%d\n", nombre_cles_arbre_r(a));
  }
  else
  {
    printf("r: %d\n", nombre_cles_arbre_r(a));
    printf("nr: %d\n", nombre_cles_arbre_nr(a));
    printf("ERREUR: La fonction non récursive et récursive ne donne pas le même nombre de clef...\n");
  }

  //clef minimale
  printf("Clef min: ");
  printf("%d\n", trouver_cle_min(a));

  //liste des clefs triées
  printf("Liste des clefs triées algo non récursif:\n");
  imprimer_liste_cle_triee_nr(a);
  printf("\nListe des clefs triées algo récursif:\n");
  imprimer_liste_cle_triee_r(a);
  printf("\n");

  //arbre pleins
  if (arbre_plein(a) == 1)
  {
    printf("L'arbre est plein.\n");
  }
  else
    printf("L'arbre n'est pas plein.\n");

  //arbre parfait
  if (arbre_parfait(a) == 1)
  {
    printf("L'arbre est parfait.\n");
  }
  else
    printf("L'arbre n'est pas parfait.\n");

  //arbre composé des etiquette sup à N
  printf("arbre composé des etiquette sup à %d:\n", N);
  Arbre_t b = rechercher_cle_sup_arbre(a, N);
  afficher_arbre(b, 0);

  //arbre composé des etiquette inf à N
  printf("arbre composé des etiquette inf à %d:\n", N);
  Arbre_t c = rechercher_cle_inf_arbre(a, N);
  afficher_arbre(c, 0);

  //union de b et c
  printf("Union des arbres b et c:\n");
  Arbre_t d = union_deux_arbres(b, c);
  afficher_arbre(d, 0);

  //intersection de a et d
  printf("Intersection des arbres a et d:\n");
  Arbre_t e = intersection_deux_arbres(a, d);
  afficher_arbre(e, 0);

  //suppression d'une clef
  printf("On supprime la clef %d de l'arbre a:\n", C);
  printf("Arbre a:\n");
  afficher_arbre(a, 0);
  printf("Arbre g:\n");
  Arbre_t g = detruire_cle_arbre(a, C);
  afficher_arbre(g, 0);
}

void testFile(){
  pfile_t file = creer_file();
  afficher_file(file);

  enfiler(file, nouveau_noeud(NULL, 1, NULL));
  afficher_file(file);

  enfiler(file, nouveau_noeud(NULL, 2, NULL));
  afficher_file(file);

  pnoeud_t n = defiler(file);
  printf("Valeur sortie : %d\n", n->cle);
  afficher_file(file);

  n = defiler(file);
  printf("Valeur sortie : %d\n", n->cle);
  afficher_file(file);

  enfiler(file, nouveau_noeud(NULL, 1, NULL));
  afficher_file(file);

  enfiler(file, nouveau_noeud(NULL, 2, NULL));
  afficher_file(file);

  enfiler(file, nouveau_noeud(NULL, 3, NULL));
  afficher_file(file);

  enfiler(file, nouveau_noeud(NULL, 4, NULL));
  afficher_file(file);

  n = defiler(file);
  printf("Valeur sortie : %d\n", n->cle);
  afficher_file(file);

  n = defiler(file);
  printf("Valeur sortie : %d\n", n->cle);
  afficher_file(file);

  n = defiler(file);
  printf("Valeur sortie : %d\n", n->cle);
  afficher_file(file);

  n = defiler(file);
  printf("Valeur sortie : %d\n", n->cle);
  afficher_file(file);
}

void testPile()
{
  ppile_t p = creer_pile();

  //pile vide
  if (pile_vide(p) == 1)
    printf("La pile créée est vide\n");
  else
    printf("ERREUR: la pile créée n'est pas vide\n");

  
  printf("affichage pile vide:\n");
  afficher_pile(p);
  printf("\n\n");

  //empiler
  printf("REMPLISSAGE DE LA PILE :\n");
  while (p->sommet != MAX_PILE_SIZE)
  {
    pnoeud_t tmp = malloc(sizeof(noeud_t));
    printf("Ajout de %d:\n", p->sommet);
    tmp->cle = p->sommet;
    tmp->fdroite = NULL;
    tmp->fgauche = NULL;
    empiler(p, tmp);
  }
  printf("La pile p:\n");
  afficher_pile(p);

  //pile pleine
  if (pile_pleine(p) == 1)
    printf("La pile est pleine\n");
  else
    printf("ERREUR: la pile n'est pas pleine\n");

  printf("\n\n");
  printf("DEPILAGE DE LA PILE :\n");
  //dépiler
  while (p->sommet != 0)
  {
    
    printf("On dépile la clef: %d.\n", p->Tab[p->sommet-1]->cle);
    depiler(p);
    afficher_pile(p);
  }
  //pile vide
  if (pile_vide(p) == 1)
    printf("La pile est vide\n");
  else
    printf("ERREUR: la pile n'est pas vide\n");
}

int main(int argc, char **argv)
{
  Arbre_t a;

  if (argc != 2)
  {
    fprintf(stderr, "il manque le parametre nom de fichier\n");
    exit(-1);
  }

  a = lire_arbre(argv[1]);
  printf("Arbre a:\n");
  afficher_arbre(a, 0);

  printf("==DEBUT TEST ARBRE==\n");
  testArbre(a);
  printf("==FIN TEST ARBRE==\n");
  /*
  printf("==DEBUT TEST FILE==\n");
  testFile();
  printf("==FIN TEST FILE==\n");
  printf("==DEBUT TEST PILE==\n");
  testPile();
  printf("==FIN TEST PILE==\n");
  */
}